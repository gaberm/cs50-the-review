/*
Luhn’s Algorithm

So what’s the secret formula? Well, most cards use an algorithm invented by Hans Peter Luhn of IBM. According to Luhn’s algorithm, you can determine if a credit card number is (syntactically) valid as follows:

    Multiply every other digit by 2, starting with the number’s second-to-last digit, and then add those products’ digits together.
    Add the sum to the sum of the digits that weren’t multiplied by 2.
    If the total’s last digit is 0 (or, put more formally, if the total modulo 10 is congruent to 0), the number is valid!

  4003-6000-0000-0014
  - -  - -  - -  - -
  8+0+1+2+0+0+0+2 = 13
  0+3+0+0+0+0+4 = 7
  Total = 20
  20 % 2 = 0

American Express uses 15-digit numbers, MasterCard uses 16-digit numbers, and Visa uses 13- and 16-digit numbers.

All American Express numbers start with 34 or 37; most MasterCard numbers start with 51, 52, 53, 54, or 55 (they also have some other potential starting numbers which we won’t concern ourselves with for this problem); and all Visa numbers start with 4.

Program’s last line of output be
AMEX\n or MASTERCARD\n or VISA\n or INVALID\n

testing :
https://developer.paypal.com/docs/payflow/payflow-pro/payflow-pro-testing/#credit-card-numbers-for-testing


pseudeocode
- prompt for input
- calculate checksum
- check for length and starting digits
- print Amex, Mastercard, Visa or INVALID
*/


#include <string.h>
#include <stdio.h>

int luhn2(const char* cc)
{
	const int m[] = {0,2,4,6,8,1,3,5,7,9}; // mapping for rule 3
	int i, odd = 1, sum = 0;

	for (i = strlen(cc); i--; odd = !odd) {
		int digit = cc[i] - '0';
		sum += odd ? digit : m[digit];
	}

	return sum % 10 == 0;
}

int luhn(char* cc)
{
	const int m[] = {0,2,4,6,8,1,3,5,7,9}; // mapping for rule 3
	int i, odd = 1, sum = 0;

	for (i = strlen(cc); i--; odd = !odd) {
		int digit = cc[i] - '0';
		sum += odd ? digit : m[digit];
	}

	return sum % 10 == 0;
}


int main()
{

	const char* cc[] = {
		"49927398716",
		"49927398717",
		"1234567812345678",
		"1234567812345670",
		0
	};
  char* number;
  _Bool flag;
  printf("Number: ");
  scanf("%s", number);
	int i;

	// for (i = 0; cc[i]; i++)
		// printf("%16s\t%s\n", cc[i], luhn(cc[i]) ? "ok" : "not ok");

  printf("%16s\t%s\n", number, luhn(number) ? "ok" : "not ok");

  flag = luhn(number);
  printf("%d\n", flag);
  if (flag == true) {
    /* code */
  } else {
    printf("Invalid\n")
  }
	return 0;
}
