#include <stdio.h>
int main()
{
    int i, j, rows;
    printf("Enter number of rows: ");
    scanf("%d", &rows);
    for (i = 1; i <= rows; ++i)
    {
        // print space
        for (j = 0; j <= rows - i; ++j)
        {
            printf("  ");
        }
        // print block
        for (j = i; j >= 1; --j)
        {
            printf("# ");
        }
        printf("\n");
    }
    return 0;
}