// #include <cs50.h>
#include <stdio.h>

//const keyword to tell the compiler
// that the value of N should never be changed by our program.
// const int N = 3;

float average(int length, int array[]);

int main(void)
{
    // int n = 3;
    int n;
    printf("Enter the Number of scores: ");
    scanf("%i\n", &n);
 
    int scores[n];
    for ( int i = 0; i < n; i++)
    {
        printf("Score %i: ", i + 1);
        // scanf("%i\n", &scores[i]);
    }
    
    // scores[0]=72;
    // scores[1]=73;
    // scores[2]=33;

    // printf("Average scores: %d\n", (scores[0]+scores[1]+scores[2]) / 3);
    printf("Average scores: %.2f\n", average(n, scores));
}

float average(int length, int array[])
{
    int sum = 0;
    for (int i = 0; i < length; i++)
    {
        sum += array[i];
    }
    return (float)sum / (float)length;
}